// src/calc.js

/**
 * Adds two numbers together
 * @param {number} a
 * @param {number} b
 * @returns {number}
 */
const add = (a, b) => a + b;

/**
 * Subtracts number from minuend
 * @param {number} minuend
 * @param {number} subtrahend
 * @returns {number} difference
 */
const subtract = (minued, subtrahend) => {
    return minued - subtrahend;
};

/**
 * Multiplies two numbers together
 * @param {number} a
 * @param {number} b
 * @returns {number}
*/
const multiply = (multiplier, multiplicant) => multiplier * multiplicant;

/**
 * Divides dividend by divisor
 * @param {number} dividend
 * @param {number} divisor
 * @returns {number} quotient
 * @throws {Error} if divisor is 0
 */
const divide = (divident, divisor) => {
    if (divisor === 0) {
        throw new Error("0 division not allowed")
    } else {
        return divident / divisor;
    }
};

export default {add, subtract, multiply, divide}