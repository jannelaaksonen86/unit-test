mkdir -p ~/unit-test
code ~/unit-test

git init
npm init -y

npm install --save express
npm install --save-dev mocha chai

echo "node_modules" > .gitignore

git remote add origin https://gitlab.com/jannelaaksonen86/unit-test
git push -u origin main

touch README.md
mkdir src test
touch src/main.js src/calc.js
mkdir public
touch public/index.html
touch test/calc.test.js